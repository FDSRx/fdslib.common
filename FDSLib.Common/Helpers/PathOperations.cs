﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Helpers
{
    /// <summary>
    /// Provides a set of methods for reading and manipulating paths.
    /// </summary>
    public static class PathOperations
    {
        /// <summary>
        /// Merges an array of file path parts (e.g. part 1 = "C:\Document\", part 2 = "\Files\"; result = "C:\Document\Files\")
        /// </summary>
        /// <param name="pathParts">An array of path parts.</param>
        /// <param name="isLastPartFile">True to indicate the ending part is a file and should NOT include a "\" at the end. False to include the "\".</param>
        /// <returns>An aboslute path</returns>
        public static string MergePaths(string[] pathParts, bool isLastPartFile)
        {
            string completedPath = String.Empty;

            if (pathParts.Length == 0)
                return null;

            for (int i = 0; i < pathParts.Length; i++)
            {
                string part = String.Empty;
                part = pathParts[i];

                completedPath += CreateStrictPath(StripOuterPathCharacters(part), false);
            }

            if (isLastPartFile)
                completedPath = StripOuterPathCharacters(completedPath);

            return completedPath;
        }

        /// <summary>
        /// Merges an array of file path parts (e.g. part 1 = "C:\Document\", part 2 = "\Files\"; result = "C:\Document\Files\")
        /// </summary>
        /// <param name="pathParts">An array of path parts.</param>
        /// <returns>An aboslute path</returns>
        public static string MergePaths(string[] pathParts)
        {
            return MergePaths(pathParts, false);
        }

        /// <summary>
        /// Merges the two paths together.
        /// </summary>
        /// <param name="path1">Starting path.</param>
        /// <param name="path2">Ending path.</param>
        /// <returns></returns>
        public static string MergePaths(string path1, string path2)
        {
            return MergePaths(new string[] { path1, path2 }, false);
        }

        /// <summary>
        /// Merges the two paths together.
        /// </summary>
        /// <param name="path1">Starting path.</param>
        /// <param name="path2">Ending path.</param>
        /// <param name="isPath2File">True to indicate path2 is a file and should NOT include a "\" at the end. False to include the "\".</param>
        /// <returns></returns>
        public static string MergePaths(string path1, string path2, bool isPath2File)
        {
            return MergePaths(new string[] { path1, path2 }, isPath2File);
        }

        /// <summary>
        /// Merges the paths together.
        /// </summary>
        /// <param name="path1">Starting path.</param>
        /// <param name="path2">Middle path.</param>
        /// <param name="path3">Ending paht.</param>
        /// <returns></returns>
        public static string MergePaths(string path1, string path2, string path3)
        {
            return MergePaths(new string[] { path1, path2, path3 }, false);
        }

        /// <summary>
        /// Merges the paths together.
        /// </summary>
        /// <param name="path1">Starting path.</param>
        /// <param name="path2">Middle path.</param>
        /// <param name="path3">Ending paht.</param>
        /// <param name="isPath3File">True to indicate path2 is a file and should NOT include a "\" at the end. False to include the "\".</param>
        /// <returns></returns>
        public static string MergePaths(string path1, string path2, string path3, bool isPath3File)
        {
            return MergePaths(new string[] { path1, path2, path3 }, isPath3File);
        }


        /// <summary>
        /// Enforces a file path to have the ending character end with "\" if it is not a direct pointer to a file.
        /// </summary>
        /// <param name="path">File path.</param>
        /// <param name="isEndOfPathFile">True to indicate the path is a direct pointer to a file.  If true, will NOT append "\" to end of the path. If false,
        /// will append "\" to end of path.
        /// </param>
        /// <returns></returns>
        public static string CreateStrictPath(string path, bool isEndOfPathFile)
        {
            string sanitizedPath = String.Empty;

            if (String.IsNullOrEmpty(path))
                return null;

            string part = StripOuterPathCharacters(path);

            if (String.IsNullOrEmpty(part))
                return null;

            path = part.Length > 0 && StringHelper.Right(path, 1) != "\\" ? path + "\\" : path;

            return path;
        }


        /// <summary>
        /// Enforces a file path to have the ending character end with "\" if it is not a direct pointer to a file.
        /// </summary>
        /// <param name="path">File path.</param>
        /// <returns></returns>
        public static string CreateStrictPath(string path)
        {
            return CreateStrictPath(path, false);
        }

        /// <summary>
        /// Strips leading and trailing "\" of path part (e.g. \DirectoryName\ becomes DirectoryName)
        /// </summary>
        /// <param name="part">Part of path.</param>
        /// <returns></returns>
        public static string StripOuterPathCharacters(string part)
        {
            if (String.IsNullOrEmpty(part))
                return String.Empty;


            bool isStartOfPath = part.Length > 0 && part.Length >= 2 && part.Substring(0, 2) == @"\\" ? true : false;

            //strip leading char
            if (!isStartOfPath)
                part = part.Length > 0 && part.Substring(0, 1) == "\\" ? (part.Length > 1 ? part.Substring(1) : "") : part;

            //strip trailing char
            part = part.Length > 0 && StringHelper.Right(part, 1) == "\\" ? (part.Length > 1 ? part.Substring(0, part.Length - 1) : "") : part;

            return part;
        }
    }
}
