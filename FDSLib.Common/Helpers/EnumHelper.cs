﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Common.Enumerations;

namespace FDSLib.Common.Helpers
{
    public static class EnumHelper
    {
        /// <summary>
        /// Converts the string into its enum representation (if applicable).
        /// </summary>
        /// <typeparam name="TEnum">Enum object.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <returns></returns>
        public static TEnum ParseEnum<TEnum>(string value) where TEnum : struct
        {
            TEnum tmp;
            if (!Enum.TryParse<TEnum>(value, true, out tmp))
            {
                tmp = new TEnum();
            }
            return tmp;
        }

        /// <summary>
        /// Converts the string into its enum representation (if applicable). If the string is unable
        /// to be converted then the defaultValue property provided will be used in its place.
        /// </summary>
        /// <typeparam name="T">Enum object.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <param name="defaultValue">The default enum value to use if the conversion is not succcessful.</param>
        /// <returns></returns>
        public static T ParseEnum<T>(string value, T defaultValue) where T : struct
        {
            try
            {
                T enumValue;
                if (!Enum.TryParse(value, true, out enumValue))
                {
                    return defaultValue;
                }

                return enumValue;
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }



    }
}
