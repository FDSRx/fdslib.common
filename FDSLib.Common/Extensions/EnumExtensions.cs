﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Extensions
{
    public static class EnumExtensions
    {
        /// <summary>
        /// Gets a specified attribute of the enumeration.
        /// </summary>
        /// <typeparam name="TAttribute">Attribute object.</typeparam>
        /// <param name="value">Enum object.</param>
        /// <returns>Returns an enumeration attribute.</returns>
        public static TAttribute GetAttribute<TAttribute>(this Enum value)
        {
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            return type.GetField(name).GetCustomAttributes(false).OfType<TAttribute>().SingleOrDefault();
        }
    }
}
