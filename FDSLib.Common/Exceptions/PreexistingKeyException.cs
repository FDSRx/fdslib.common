﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Exceptions
{
    [Serializable]
    public class PreexistingKeyException : Exception
    {
        private const string DefaultErrorMessage = "The key already exists.";

        /// <summary>
        /// Initializes a new instance of the PreexistingKeyException class.
        /// </summary>
        public PreexistingKeyException()
            : base(DefaultErrorMessage)
        { }

        /// <summary>
        /// Initializes a new instance of the PreexistingKeyException class.
        /// </summary>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner
        /// exception is specified.</param>
        public PreexistingKeyException(Exception innerException)
            : base(DefaultErrorMessage, innerException)
        { }

        /// <summary>
        /// Initializes a new instance of the PreexistingKeyException class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public PreexistingKeyException(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the PreexistingKeyException class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner
        /// exception is specified.</param>
        public PreexistingKeyException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
