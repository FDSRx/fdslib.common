﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FDSLib.Common.Extensions
{
    public static class ArrayObjectExtensions
    {

        /// <summary>
        /// Gets the value at the specified position in the multidimensional System.Array. The indexes are specified as an array of 64-bit
        /// integers. Returns the default string value of null if the index is out of bounds.
        /// </summary>
        /// <param name="arr">The array.</param>
        /// <param name="index">A 32-bit integer that respesents the position of the System.Array element to get.</param>
        /// <returns></returns>
        public static string GetValueOrDefault(this string[] arr, int index)
        {
            try
            {
                return arr[index];
            }
            catch
            {
                return default(string);
            }

        }

        /// <summary>
        /// Gets the value at the specified position in the multidimensional System.Array. The indexes are specified as an array of 64-bit
        /// integers. Returns the provided default value if the index is out of bounds.
        /// </summary>
        /// <param name="arr">The array.</param>
        /// <param name="index">A 32-bit integer that respesents the position of the System.Array element to get.</param>
        /// <param name="defaultValue">The default value to return if the array is out of bounds.</param>
        /// <returns></returns>
        public static string GetValueOrDefault(this string[] arr, int index, string defaultValue)
        {
            try
            {
                return arr[index];
            }
            catch
            {
                return defaultValue;
            }

        }

        /// <summary>
        /// Gets the value at the specified position in the multidimensional System.Array. The indexes are specified as an array of 64-bit
        /// integers. Returns the a default of String.Empty if the index is out of bounds.
        /// </summary>
        /// <param name="arr">The array.</param>
        /// <param name="index">A 32-bit integer that respesents the position of the System.Array element to get.</param>
        /// <returns></returns>
        public static string GetValueOrEmpty(this string[] arr, int index)
        {
            try
            {
                return arr[index];
            }
            catch
            {
                return String.Empty;
            }

        }

        /// <summary>
        /// Gets the value at the specified position in the multidimensional System.Array. The indexes are specified as an array of 64-bit
        /// integers. Returns the a default of String.Empty if the index is out of bounds.
        /// </summary>
        /// <param name="arr">The array.</param>
        /// <param name="index">A 32-bit integer that respesents the position of the System.Array element to get.</param>
        /// <param name="defaultValue">The default value to return if the array is out of bounds.</param>
        /// <returns></returns>
        public static string GetValueOrEmpty(this string[] arr, int index, string defaultValue)
        {
            try
            {
                return arr[index];
            }
            catch
            {
                return defaultValue;
            }

        }






    }
}
