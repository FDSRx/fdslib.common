﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Linq.Expressions;

namespace FDSLib.Common.Helpers
{
    public class EntityHelper
    {
        /// <summary>
        /// Returns a string representation of the attribute
        /// </summary>
        /// <param name="propertyExpression">Entity expression.</param>
        public static string GetPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            if (propertyExpression == null)
            {
                throw new ArgumentNullException("propertyExpression");
            }

            return (propertyExpression.Body as MemberExpression).Member.Name;
        }

    }
}
