﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Helpers
{
    /// <summary>
    /// Provides a set of methods for directory reading and writing.
    /// </summary>
    public class DirectoryOperations
    {
        /// <summary>
        /// Creates all directories and sub directories in the specified path.
        /// </summary>
        /// <param name="path">The directory path to create.</param>
        public static void CreateDirectory(string path)
        {
            if (!String.IsNullOrEmpty(path) && !Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        /// <summary>
        /// Creates all directories and sub directories in the specified path, applying the specified Windows security.
        /// </summary>
        /// <param name="path">The directory path to create.</param>
        /// <param name="directorySecurity">The access control to apply to the directory.</param>
        public static void CreateDirectory(string path, System.Security.AccessControl.DirectorySecurity directorySecurity)
        {
            if (!String.IsNullOrEmpty(path) && !Directory.Exists(path))
                Directory.CreateDirectory(path, directorySecurity);
        }




    }





}
