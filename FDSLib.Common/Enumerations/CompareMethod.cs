﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Enumerations
{
    /// <summary>
    /// Compare methods.
    /// </summary>
    public enum CompareMethod
    {
        /// <summary>
        /// Ignores character casing during compare operation (e.g. HelLo WoRld = Hello World).
        /// </summary>
        IgnoreCase = 1,

        /// <summary>
        /// Characters casing is critical during a compare operation (e.g. HelLo WoRld != Hello World).
        /// </summary>
        CaseSensitive = 2
    }
}
