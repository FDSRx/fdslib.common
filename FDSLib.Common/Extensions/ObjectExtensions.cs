﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Security.Policy;
using System.Text.RegularExpressions;
using System.Web;
using FDSLib.Common.Helpers;

namespace FDSLib.Common.Extensions
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// Casts the object to the provided type(Of T) (e.g. (string)object). If the cast is unable to be performed then the types
        /// default value will be returned.
        /// </summary>
        /// <typeparam name="T">Type to cast.</typeparam>
        /// <param name="obj">The object to cast.</param>
        /// <returns></returns>
        public static T CastOrDefault<T>(this Object obj)
        {
            try
            {
                return (T)obj;
            }
            catch
            {
            }

            return default(T);
        }

        /// <summary>
        /// Serializes the object into a JSON object.
        /// </summary>
        /// <param name="obj">The object to serialize</param>
        /// <returns></returns>
        public static string ToJson(this Object obj)
        {
            string json = SerializationHelper.SerializeToJson(obj);

            return json;
        }

        /// <summary>
        /// Serializes the object into a JSON string object.
        /// </summary>
        /// <param name="obj">The object to serialize</param>
        /// <returns></returns>
        public static string ToJsonOrEmpty(this Object obj)
        {
            try
            {
                return ToJson(obj);
            }
            catch
            {
                return String.Empty;
            }
        }


        /// <summary>
        /// Serializes the object into a XML string object.
        /// </summary>
        /// <param name="obj">The object to serialize.</param>
        /// <returns></returns>
        public static string ToXml(this Object obj)
        {
            string xml = SerializationHelper.SerializeToXml(obj);

            return xml;
        }

        /// <summary>
        /// Serializes the object into a XML string object. If the object is unable to be serialized then an
        /// empty string will be returned.
        /// </summary>
        /// <param name="obj">The object to serialize.</param>
        /// <returns></returns>
        public static string ToXmlOrEmpty(this Object obj)
        {
            try
            {
                return ToXml(obj);
            }
            catch
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Serializes the object into a XML string object. If the object is unable to be serialized then a null value
        /// will be returned.
        /// </summary>
        /// <param name="obj">The object to serialize.</param>
        /// <returns></returns>
        public static string ToXmlOrDefault(this Object obj)
        {
            try
            {
                return ToXml(obj);
            }
            catch
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Adds a property to the anonymous object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="name">The name of the property.</param>
        /// <param name="value">The value of the property.</param>
        /// <returns></returns>
        public static IDictionary<string, object> AddProperty(this object obj, string name, object value)
        {
            var dictionary = obj.ToDictionary();
            dictionary.Add(name, value);
            return dictionary;
        }

        /// <summary>
        /// Returns an anonymous object as a dictionary(Of string, object).
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public static IDictionary<string, object> ToDictionary(this object obj)
        {
            IDictionary<string, object> result = new Dictionary<string, object>();
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(obj);
            foreach (PropertyDescriptor property in properties)
            {
                result.Add(property.Name, property.GetValue(obj));
            }
            return result;
        }
    }
}