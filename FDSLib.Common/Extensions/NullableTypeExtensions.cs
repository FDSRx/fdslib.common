﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Extensions
{
    public static class NullableTypeExtensions
    {
        /// <summary>
        /// Retrieves the value of the current System.Nullable(T) object, or null if the object is null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">Nullable object.</param>
        /// <returns></returns>
        public static T? GetValue<T>(this Nullable<T> obj) where T : struct
        {
            if (obj == null || obj.HasValue == false)
            {
                return null;
            }

            return obj.Value;
        }

    }


}
