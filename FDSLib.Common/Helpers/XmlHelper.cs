﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using FDSLib.Common.Exceptions;
using FDSLib.Common.Formatters;


namespace FDSLib.Common.Helpers
{
    public class XmlHelper
    {
        /// <summary>
        /// Removes XML namespace from schema
        /// </summary>
        /// <param name="xml">Xml string data.</param>
        public static string StripNamespace(string xml)
        {
            //Regex below finds strings that start with xmlns, may or may not have :and some text, then continue with =
            //and ", have a streach of text that does not contain quotes and end with ". similar, will happen to an attribute
            // that starts with xsi.
            string strXMLPattern = @"xmlns(:\w+)?=""([^""]+)""|xsi(:\w+)?=""([^""]+)""";
            xml = Regex.Replace(xml, strXMLPattern, "");

            return xml;

        }

        /// <summary>
        /// Removes Nil attributes from XML schema
        /// </summary>
        /// <param name="xml">Xml string data.</param>
        public static string StripNils(string xml)
        {
            string strXMLPattern = @"(i|p3)(:\w+)?=""([^""]+)""";
            xml = Regex.Replace(xml, strXMLPattern, "");

            return xml;

        }

        /// <summary>
        /// Loads the xml string into a queryable linq XDocument object.
        /// </summary>
        /// <param name="xml">Xml string.</param>
        /// <param name="stripNamespaces">True if the xml string should be stripped of any namespaces; Otherwise, the namespaces are left as is. 
        /// The default is to remove all namespaces.</param>
        /// <param name="stripNils">True if the xml string should be stripped of all elements that are set to Nil; Otherwise, the Nil elements are left as is.
        /// The default is to remove all Nils from the elements.</param>
        /// <returns></returns>
        public static XDocument LoadXDocument(string xml, bool stripNamespaces = true, bool stripNils = true)
        {
            xml = stripNamespaces ? StripNamespace(xml) : xml;
            xml = stripNils ? StripNils(xml) : xml;

            XDocument document = XDocument.Parse(xml);

            return document;
        }

        /// <summary>
        /// Returns an Xml string value that contains the xml element and its corresponding value. Will return an empty string if it cannot write the element/value.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string CreateElementWithValue(string element, string value)
        {
            string xmlString = String.Empty;

            try
            {
                //we want the writter to only write the tags. We don't need to worry about the document structure.
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.ConformanceLevel = ConformanceLevel.Fragment;
                settings.OmitXmlDeclaration = true;

                //let's write the data in UTF-8
                using (TextWriter writer = new XmlStringWriter())
                {
                    using (XmlWriter xmlWriter = XmlWriter.Create(writer, settings))
                    {
                        xmlWriter.WriteElementString(element, value);
                    }

                    xmlString = writer.ToString();
                }
            }
            catch
            {
                //return an empty string if the writer fails
                xmlString = String.Empty;
            }

            return xmlString;
        }

        /// <summary>
        /// Serializes the specified entity (class object) into an xml string.
        /// </summary>
        /// <typeparam name="T">Entity object.</typeparam>
        /// <param name="entity">Populated entity to be serialized.</param>
        /// <returns></returns>
        public static string SerializeEntity<T>(T entity) where T : class
        {
            string xml = String.Empty;

            try
            {
                //create optional namespace
                XmlSerializerNamespaces xmlNameSpace = new XmlSerializerNamespaces();
                xmlNameSpace.Add("", "");

                XmlSerializer xserial = new XmlSerializer(typeof(T));
                MemoryStream memStream = new MemoryStream();
                XmlTextWriter txtWriter = new XmlTextWriter(memStream, System.Text.Encoding.UTF8);
                xserial.Serialize(txtWriter, entity, xmlNameSpace);

                xml = System.Text.Encoding.UTF8.GetString(memStream.ToArray());

                memStream.Close();
                memStream.Dispose();
                txtWriter.Close();
            }
            catch (Exception ex)
            {
                XElementException xex = new XElementException(ex);
                xml = xex.ToString();
            }

            return xml;
        }

        /// <summary>
        /// Returns the first string value of the requested xml node from the xml string.  If the node is not found or an error was encountered
        /// then the default value of null is returned.
        /// </summary>
        /// <param name="xml">The xml string.</param>
        /// <param name="nodeName">The name of the xml node to parse.</param>
        /// <returns></returns>
        public static string GetFirstOrDefaultValue(string xml, string nodeName)
        {
            string result = null;

            try
            {
                var sr = new StringReader(xml);
                var xmlDoc = new XmlDocument();

                bool isFound = false;

                xmlDoc.Load(sr);
                var reader = new XmlNodeReader(xmlDoc);
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (reader.Name.Trim().ToLower() == nodeName.Trim().ToLower())
                            {
                                reader.Read();
                                result = reader.HasValue ? reader.Value : null;
                                isFound = true;
                            }
                            break;
                    }

                    //If I have found a value then exit the loop.
                    if (isFound)
                    {
                        break;
                    }
                }

                reader.Close();
                sr.Close();
                sr.Dispose();
            }
            catch (Exception)
            {
                result = null;
            }

            return result;

        }

        /// <summary>
        /// Returns the first value (Of Type T) from the parsed xml string for the specified node.  If the node is not found or an error was encountered
        /// then the default value of the specified type is returned.
        /// </summary>
        /// <param name="xml">The xml string.</param>
        /// <param name="nodeName">The name of the xml node to parse.</param>
        /// <returns></returns>
        public static T GetFirstOrDefaultValue<T>(string xml, string nodeName)
        {
            try
            {
                var obj = GetFirstOrDefaultValue(xml, nodeName);

                return ObjectHelper.ConvertToType<T>(obj);
            }
            catch
            {
                return default(T);
            }
        }

        /// <summary>
        /// Returns the first value (Of Type T) from the parsed xml string for the specified node.  If the node is not found or an error was encountered
        /// then null will be returned.
        /// </summary>
        /// <param name="xml">The xml string.</param>
        /// <param name="nodeName">The name of the xml node to parse.</param>
        /// <returns></returns>
        public static T? GetFirstOrNullValue<T>(string xml, string nodeName) where T : struct
        {
            if (xml == null)
            {
                return null;
            }

            if (nodeName == null)
            {
                return null;
            }

            try
            {
                var obj = GetFirstOrDefaultValue(xml, nodeName);

                return ObjectHelper.ConvertToType<T>(obj);
            }
            catch
            {
                return null;
            }
        }

    }


}
