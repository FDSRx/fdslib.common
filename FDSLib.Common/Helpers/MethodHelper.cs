﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Helpers
{

    public static class MethodHelper
    {
        /// <summary>
        /// Executes a method or block of code within a try catch block. If an exception is encountered, the error will be ignored and
        /// the default value of the method will be returned in its place.
        /// </summary>
        /// <typeparam name="TResult">The result type.</typeparam>
        /// <param name="method">The method or block of code to be executed.</param>
        /// <returns></returns>
        public static TResult InvokeWithTryCatch<TResult>(Func<TResult> method)
        {
            try
            {
                return method();
            }
            catch
            {
                return default(TResult);
            }
        }

        /// <summary>
        /// Executes a method or block of code within a try catch block. If an exception is encountered, the error will be ignored and
        /// the provided catch result value will be returned in its place.
        /// </summary>
        /// <typeparam name="TResult">The result type.</typeparam>
        /// <param name="method">The method or block of code to be executed.</param>
        /// <param name="catchResult">The value to be returned if the catch block is executed.</param>
        /// <returns></returns>
        public static TResult InvokeWithTryCatch<TResult>(Func<TResult> method, TResult catchResult)
        {
            try
            {
                return method();
            }
            catch
            {
                return catchResult;
            }
        }


    }


}
