﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Helpers
{
    /// <summary>
    /// Various methods that help with Image object creation, manipulation, etc.
    /// </summary>
    public static class ImageHelper
    {
        /// <summary>
        /// Converts the current image object into a byte array.
        /// </summary>
        /// <param name="image">An Image object.</param>
        /// <returns></returns>
        public static byte[] ToByteArray(Image image)
        {
            if (image == null)
            {
                return default(byte[]);
            }

            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, ImageFormat.Png);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Converts the current image object into a byte array.
        /// </summary>
        /// <param name="image">An Image object.</param>
        /// <param name="format">The format of the image.</param>
        /// <returns></returns>
        public static byte[] ToByteArray(Image image, ImageFormat format)
        {
            if (image == null)
            {
                return default(byte[]);
            }

            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, format);
                return ms.ToArray();
            }
        }
    }
}
