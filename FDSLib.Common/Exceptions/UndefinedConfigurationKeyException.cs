﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Exceptions
{
    [Serializable]
    public class UndefinedConfigurationKeyException : Exception
    {
        private const string DefaultErrorMessage = "No configuration keys have been provided.";

        /// <summary>
        /// Initializes a new instance of the UndefinedConfigurationKeyException class.
        /// </summary>
        public UndefinedConfigurationKeyException()
            : base(DefaultErrorMessage)
        { }

        /// <summary>
        /// Initializes a new instance of the UndefinedConfigurationKeyException class.
        /// </summary>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner
        /// exception is specified.</param>
        public UndefinedConfigurationKeyException(Exception innerException)
            : base(DefaultErrorMessage, innerException)
        { }

        /// <summary>
        /// Initializes a new instance of the UndefinedConfigurationKeyException class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public UndefinedConfigurationKeyException(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the UndefinedConfigurationKeyException class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner
        /// exception is specified.</param>
        public UndefinedConfigurationKeyException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
