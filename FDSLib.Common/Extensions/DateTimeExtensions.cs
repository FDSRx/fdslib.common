﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Extensions
{
    public static class DateTimeExtensions
    {
        /*
         * Remarks:
         * The following section comprises a list of DateTime object functions that convert the DateTime object into a
         * short date string.
         */
         
        /// <summary>
        /// Converts a System.DateTime object into a short date string (e.g. 1/21/2014 10:33 PM = 1/21/2014).  If the object is null,
        /// then the System.DateTime.MinValue will be returned as a short date string.
        /// </summary>
        /// <returns></returns>
        public static string ToShortDateStringOrDefault(this DateTime dte)
        {
            if (dte == null)
            {
                return DateTime.MinValue.ToShortDateString();
            }

            return dte.ToShortDateString();
        }

        /// <summary>
        /// Converts a nullable System.DateTime object into a short date string (e.g. 1/21/2014 10:33 PM = 1/21/2014).  If the object is null
        /// or does not have a value, then the System.DateTime.MinValue will be returned as a short date string.
        /// </summary>
        /// <returns></returns>
        public static string ToShortDateStringOrDefault(this Nullable<DateTime> dte)
        {
            if (dte == null || !dte.HasValue)
            {
                return DateTime.MinValue.ToShortDateString();
            }

            return dte.Value.ToShortDateString();
        }

        /// <summary>
        /// Converts a System.DateTime object into a short date string (e.g. 1/21/2014 10:33 PM = 1/21/2014).  If the object is null,
        /// then the supplied default date will be returned as a short date string.
        /// </summary>
        /// <returns></returns>
        public static string ToShortDateStringOrDefault(this DateTime dte, DateTime defaultDate)
        {
            if (dte == null)
            {
                if (defaultDate == null)
                {
                    throw new ArgumentNullException("defaultDate");
                }

                return defaultDate.ToShortDateString();
            }

            return dte.ToShortDateString();
        }

        /// <summary>
        /// Converts a nullable System.DateTime object into a short date string (e.g. 1/21/2014 10:33 PM = 1/21/2014).  If the object is null
        /// or does not have a value, then the supplied default date will be returned as a short date string.
        /// </summary>
        /// <returns></returns>
        public static string ToShortDateStringOrDefault(this Nullable<DateTime> dte, Nullable<DateTime> defaultDate)
        {
            if (dte == null || !dte.HasValue)
            {
                if (defaultDate != null && defaultDate.HasValue)
                {
                    return defaultDate.Value.ToShortDateString();
                }
                else
                {
                    return null;
                }
            }

            return dte.Value.ToShortDateString();
        }

        /// <summary>
        /// Converts a System.DateTime object into a short date string (e.g. 1/21/2014 10:33 PM = 1/21/2014).  If the object is null,
        /// then the indicated System.DateTime.MaxValue or System.DateTime.MinValue will be retured as a short date string.
        /// </summary>
        /// <param name="dte">DateTime object.</param>
        /// <param name="isMaxDateDefault">Indicates whether the default result should be returned as the System.DateTime.MaxValue or System.DateTime.MinValue.</param>
        /// <returns></returns>
        public static string ToShortDateStringOrDefault(this DateTime dte, bool isMaxDateDefault)
        {
            if (dte == null)
            {
                
                if (isMaxDateDefault)
                {
                    return DateTime.MaxValue.ToShortDateString();
                }
                else
                {
                    return DateTime.MinValue.ToShortDateString();
                }
            }

            return dte.ToShortDateString();
        }

        /// <summary>
        /// Converts a nullable System.DateTime object into a short date string (e.g. 1/21/2014 10:33 PM = 1/21/2014).  If the object is null
        /// or does not have a value, then the indicated System.DateTime.MaxValue or System.DateTime.MinValue will be retured as a short date string.
        /// </summary>
        /// <param name="dte">DateTime object.</param>
        /// <param name="isMaxDateDefault">Indicates whether the default result should be returned as the System.DateTime.MaxValue or System.DateTime.MinValue.</param>
        /// <returns></returns>
        public static string ToShortDateStringOrDefault(this Nullable<DateTime> dte, bool isMaxDateDefault)
        {
            if (dte == null || !dte.HasValue)
            {
                if (isMaxDateDefault)
                {
                    return DateTime.MaxValue.ToShortDateString();
                }
                else
                {
                    return DateTime.MinValue.ToShortDateString();
                }
            }

            return dte.Value.ToShortDateString();
        }

        /// <summary>
        /// Converts a System.DateTime object into a short date string (e.g. 1/21/2014 10:33 PM = 1/21/2014).  If the object is null,
        /// then a System.String.Empty is returned.
        /// </summary>
        /// <param name="dte">DateTime object.</param>
        /// <returns></returns>
        public static string ToShortDateStringOrEmpty(this DateTime dte)
        {
            if (dte == null)
            {
                return String.Empty;
            }

            return dte.ToShortDateString();
        }

        /// <summary>
        /// Converts a nullable System.DateTime object into a short date string (e.g. 1/21/2014 10:33 PM = 1/21/2014).  If the object is null
        /// or does not have a value, then a System.String.Empty is returned.
        /// </summary>
        /// <param name="dte">DateTime object.</param>
        /// <returns></returns>
        public static string ToShortDateStringOrEmpty(this Nullable<DateTime> dte)
        {
            if (dte == null || !dte.HasValue)
            {
                return String.Empty;
            }

            return dte.Value.ToShortDateString();
        }

        /// <summary>
        /// Converts a System.DateTime object string into a short date string (e.g. 1/21/2014 10:33 PM = 1/21/2014).  If the object is null,
        /// then null is returned.
        /// </summary>
        /// <param name="dte"></param>
        /// <returns></returns>
        public static string ToShortDateStringOrNull(this DateTime dte)
        {
            if (dte == null)
            {
                return null;
            }

            return dte.ToShortDateString();

        }

        /// <summary>
        /// Converts a nullable System.DateTime object string into a short date string (e.g. 1/21/2014 10:33 PM = 1/21/2014).  If the object is null
        /// or does not have a value then null is returned.
        /// </summary>
        /// <param name="dte"></param>
        /// <returns></returns>
        public static string ToShortDateStringOrNull(this Nullable<DateTime> dte)
        {
            if (dte == null || !dte.HasValue)
            {
                return null;
            }

            return dte.Value.ToShortDateString();

        }


        /*
        * Remarks:
        * The following section comprises a list of DateTime object functions that convert the DateTime object into a
        * short time string.
        */

        /// <summary>
        /// Converts a System.DateTime object into a short time string (e.g. 1/21/2014 10:33 PM = 10:33 PM).  If the object is null,
        /// then the System.DateTime.MinValue will be returned as a short time string.
        /// </summary>
        /// <returns></returns>
        public static string ToShortTimeStringOrDefault(this DateTime dte)
        {
            if (dte == null)
            {
                return DateTime.MinValue.ToShortDateString();
            }

            return dte.ToShortTimeString();
        }

        /// <summary>
        /// Converts a nullable System.DateTime object into a short time string (e.g. 1/21/2014 10:33 PM = 10:33 PM).  If the object is null
        /// or does not have a value, then the System.DateTime.MinValue will be returned as a short time string.
        /// </summary>
        /// <returns></returns>
        public static string ToShortTimeStringOrDefault(this Nullable<DateTime> dte)
        {
            if (dte == null || !dte.HasValue)
            {
                return DateTime.MinValue.ToShortTimeString();
            }

            return dte.Value.ToShortTimeString();
        }

        /// <summary>
        /// Converts a System.DateTime object into a short time string (e.g. 1/21/2014 10:33 PM = 10:33 PM).  If the object is null,
        /// then the supplied default date will be returned as a short time string.
        /// </summary>
        /// <returns></returns>
        public static string ToShortTimeStringOrDefault(this DateTime dte, DateTime defaultDate)
        {
            if (dte == null)
            {
                if (defaultDate == null)
                {
                    throw new ArgumentNullException("defaultDate");
                }

                return defaultDate.ToShortTimeString();
            }

            return dte.ToShortTimeString();
        }

        /// <summary>
        /// Converts a nullable System.DateTime object into a short time string (e.g. 1/21/2014 10:33 PM = 10:33 PM).  If the object is null
        /// or does not have a value, then the supplied default date will be returned as a short time string.
        /// </summary>
        /// <returns></returns>
        public static string ToShortTimeStringOrDefault(this Nullable<DateTime> dte, Nullable<DateTime> defaultDate)
        {
            if (dte == null || !dte.HasValue)
            {
                if (defaultDate != null && defaultDate.HasValue)
                {
                    return defaultDate.Value.ToShortTimeString();
                }
                else
                {
                    return null;
                }
            }

            return dte.Value.ToShortTimeString();
        }

        /// <summary>
        /// Converts a System.DateTime object into a short time string (e.g. 1/21/2014 10:33 PM = 10:33 PM ).  If the object is null,
        /// then a System.String.Empty is returned.
        /// </summary>
        /// <param name="dte">DateTime object.</param>
        /// <returns></returns>
        public static string ToShortTimeStringOrEmpty(this DateTime dte)
        {
            if (dte == null)
            {
                return String.Empty;
            }

            return dte.ToShortTimeString();
        }

        /// <summary>
        /// Converts a nullable System.DateTime object into a short time string (e.g. 1/21/2014 10:33 PM = 10:33 PM).  If the object is null
        /// or does not have a value, then a System.String.Empty is returned.
        /// </summary>
        /// <param name="dte">DateTime object.</param>
        /// <returns></returns>
        public static string ToShortTimeStringOrEmpty(this Nullable<DateTime> dte)
        {
            if (dte == null || !dte.HasValue)
            {
                return String.Empty;
            }

            return dte.Value.ToShortTimeString();
        }

        /// <summary>
        /// Converts a System.DateTime object string into a short time string (e.g. 1/21/2014 10:33 PM = 10:33 PM).  If the object is null,
        /// then null is returned.
        /// </summary>
        /// <param name="dte"></param>
        /// <returns></returns>
        public static string ToShortTimeStringOrNull(this DateTime dte)
        {
            if (dte == null)
            {
                return null;
            }

            return dte.ToShortTimeString();

        }

        /// <summary>
        /// Converts a nullable System.DateTime object string into a short time string (e.g. 1/21/2014 10:33 PM = 10:33 PM).  If the object is null
        /// or does not have a value then null is returned.
        /// </summary>
        /// <param name="dte"></param>
        /// <returns></returns>
        public static string ToShortTimeStringOrNull(this Nullable<DateTime> dte)
        {
            if (dte == null || !dte.HasValue)
            {
                return null;
            }

            return dte.Value.ToShortTimeString();

        }

        /*
        * Remarks:
        * The following section comprises a list of DateTime object functions that convert the DateTime object into a
        * short date and time string.
        */

        /// <summary>
        /// Converts a System.DateTime object into a short date and time string (e.g. 1/21/2014 10:33 PM = 1/21/2014 10:33 PM ).  If the object is null,
        /// then a System.String.Empty is returned.
        /// </summary>
        /// <param name="dte">DateTime object.</param>
        /// <returns></returns>
        public static string ToShortDateTimeString(this DateTime dte)
        {
            if (dte == null)
            {
                return String.Empty;
            }

            return dte.ToShortDateString() + " " + dte.ToShortTimeString();
        }

        /// <summary>
        /// Converts a System.DateTime object into a short date and time string (e.g. 1/21/2014 10:33 PM = 1/21/2014 10:33 PM ).  If the object is null,
        /// then a System.String.Empty is returned.
        /// </summary>
        /// <param name="dte">DateTime object.</param>
        /// <returns></returns>
        public static string ToShortDateTimeStringOrEmpty(this DateTime dte)
        {
            if (dte == null)
            {
                return String.Empty;
            }

            return dte.ToShortDateString() + " " + dte.ToShortTimeString();
        }

        /// <summary>
        /// Converts a nullable System.DateTime object into a short date and time string (e.g. 1/21/2014 10:33 PM = 10:33 PM).  If the object is null
        /// or does not have a value, then a System.String.Empty is returned.
        /// </summary>
        /// <param name="dte">DateTime object.</param>
        /// <returns></returns>
        public static string ToShortDateTimeStringOrEmpty(this Nullable<DateTime> dte)
        {
            if (dte == null || !dte.HasValue)
            {
                return String.Empty;
            }

            return dte.Value.ToShortDateString() + " " + dte.Value.ToShortTimeString();
        }


    }




}
