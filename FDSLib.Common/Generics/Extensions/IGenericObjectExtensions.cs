﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FDSLib.Common.Generics
{
    public static class IGenericObjectExtensions
    {
        /// <summary>
        /// Returns the current object.  If the object is null, then a new object (new obj()) will be returned in its place.
        /// </summary>
        /// <typeparam name="T">TypeOf(T)</typeparam>
        /// <param name="obj">TypeOf(T) object.</param>
        /// <returns></returns>
        public static T RetrieveOrNew<T>(IRetrievable<T> obj) where T : class, new()
        {
            if (obj == null)
            {
                return new T();
            }

            return (T)obj;
        }

        /// <summary>
        /// Returns the current object.  If the object is null, then a new object (new obj()) will be returned in its place.
        /// </summary>
        /// <typeparam name="T">TypeOf(T)</typeparam>
        /// <param name="obj">TypeOf(T) object.</param>
        /// <returns></returns>
        public static T FetchOrNew<T>(IFetchable<T> obj) where T : class, new()
        {
            if (obj == null)
            {
                return new T();
            }

            return (T)obj;
        }
    }
}
