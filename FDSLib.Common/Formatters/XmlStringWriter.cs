﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Formatters
{
    /// <summary>
    /// Wrapper around the string writer object to format as UTF-8 encoding.
    /// </summary>
    public class XmlStringWriter : StringWriter
    {
        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }
}
