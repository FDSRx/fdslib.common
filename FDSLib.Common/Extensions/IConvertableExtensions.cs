﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Extensions
{
    public static class IConvertableExtensions
    {
        /// <summary>
        /// Converts the IConvertible object to the specified type.
        /// </summary>
        /// <typeparam name="T">Conversion type.</typeparam>
        /// <param name="obj">IConvertible object.</param>
        /// <returns></returns>
        public static T ToType<T>(this IConvertible obj)
        {
            var t = typeof(T);
            var u = Nullable.GetUnderlyingType(t);

            if (u != null)
            {
                if (obj == null)
                    return default(T);

                return (T)Convert.ChangeType(obj, u);
            }
            
            return (T)Convert.ChangeType(obj, typeof(T));
        }

        /// <summary>
        /// Converts the IConvertible object to the specified type. If the type is unable to be convereted then its default will be returned.
        /// </summary>
        /// <typeparam name="T">Conversion type.</typeparam>
        /// <param name="obj">IConvertible object.</param>
        /// <returns></returns>
        public static T ToTypeOrDefault<T>(this IConvertible obj)
        {
            try
            {
                return ToType<T>(obj);
            }
            catch
            {
                return default(T);
            }
        }

        /// <summary>
        /// Converts the IConvertible object to the specified type.  A return value indicates whether the conversion succeeded.
        /// </summary>
        /// <typeparam name="T">Conversion type.</typeparam>
        /// <param name="obj">IConvertible object.</param>
        /// <param name="newObj">Outputs IConvertible object converted to specified type.</param>
        /// <returns></returns>
        public static bool TryToType<T>(this IConvertible obj, out T newObj)
        {
            try
            {
                newObj = ToType<T>(obj);
                return true;
            }
            catch
            {
                newObj = default(T);
                return false;
            }
        }

        /// <summary>
        /// Converts the IConvertible object to the specified type.  Returns the alternate object if the specified object is unable to convert.
        /// </summary>
        /// <typeparam name="T">Conversion type.</typeparam>
        /// <param name="obj">IConvertible object.</param>
        /// <param name="alternate">Alternate object that will be returned in place of a failed conversion.</param>
        /// <returns></returns>
        public static T ToTypeOrAlternate<T>(this IConvertible obj, T alternate)
        {
          try
          {
              return ToType<T>(obj);
          }
          catch
          {
              return alternate;
          }
        }

        /// <summary>
        /// Converts the IConvertible object to the specified type.  Outputs the alternate object if the specified object is unable to convert.
        /// A return value indicates whether the conversion succeeded.
        /// </summary>
        /// <typeparam name="T">Conversion type.</typeparam>
        /// <param name="obj">IConvertible object.</param>
        /// <param name="newObj">Outputs IConvertible object converted to specified type.</param>
        /// <param name="alternate">Alternate object that will be returned in place of a failed conversion.</param>
        /// <returns></returns>
        public static bool TryToTypeOrAlternate<T>(this IConvertible obj, out T newObj, T alternate)
        {
            try
            {
                newObj = ToType<T>(obj);
                return true;
            }
            catch
            {
                newObj = alternate;
                return false;
            }
        }


        /// <summary>
        /// Converts the IConvertible object to the specified type.  Returns a null object type if the specified type is unable to convert.
        /// </summary>
        /// <typeparam name="T">Conversion type.</typeparam>
        /// <param name="obj">IConvertible object.</param>
        /// <returns></returns>
        public static T? ToTypeOrNull<T>(this IConvertible obj) where T : struct
        {
            try
            {
                return ToType<T>(obj);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Converts the IConvertible object to the specified type.  A return value indicates whether the conversion succeeded.
        /// </summary>
        /// <typeparam name="T">Conversion type.</typeparam>
        /// <param name="obj">IConvertible object.</param>
        /// <param name="newObj">Outputs IConvertible object converted to specified type.</param>
        /// <returns></returns>
        public static bool TryToTypeOrNull<T>(this IConvertible obj, out T? newObj) where T : struct
        {
            try
            {
                newObj = ToType<T>(obj);
                return true;
            }
            catch
            {
                newObj = null;
                return false;
            }
        }



    }





}
