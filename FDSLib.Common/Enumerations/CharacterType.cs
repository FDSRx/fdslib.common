﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Common.Enumerations
{
    /// <summary>
    /// The various types of character combinations.
    /// </summary>
    public enum CharacterType
    {
        /// <summary>
        /// Numeric characters only.
        /// </summary>
        Numeric = 1,

        /// <summary>
        /// A combination of numeric and alpha characters.
        /// </summary>
        AlphaNumeric = 2,

        /// <summary>
        /// Alpha characters only.
        /// </summary>
        Alpha = 3
    }
}
